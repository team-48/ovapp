package ovapp;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class DataHandler 
{
	
	//get users from JSON file and put it into a JSONArray
	private JSONArray getUsers() throws FileNotFoundException, IOException, ParseException
	{
		JSONParser parser = new JSONParser();
		String loc = new String("ovapp/src/data/users.json");
		
		JSONArray usersArray = (JSONArray)parser.parse(new FileReader(loc));
		return usersArray;
	}
	
	//get chosen profile data from JSONArray
	public Profile getProfile(String id) throws FileNotFoundException, IOException, ParseException
	{
		JSONArray usersArray = getUsers();
		Profile pro = new Profile();
	
		for(Object profile : usersArray)
		{
			JSONObject profileObj = (JSONObject) profile;
			String person = (String) profileObj.get("id");
			
			//if id matches the id from the user, create and return the profile object
			if(person.equalsIgnoreCase(id))
			{
				String profileID = (String) profileObj.get("id");
				String firstName = (String) profileObj.get("firstname");
				String lastName = (String) profileObj.get("lastname");
				String adress = (String) profileObj.get("adress");
				String city = (String) profileObj.get("city");
				pro = new Profile(profileID, firstName, lastName, adress, city);
			}
		}
		return pro;
	}
	
	//get a user's travel history or saved routes depending on boolean
	public ArrayList<Route> getRoutes(String id, boolean favorite) throws FileNotFoundException, IOException, ParseException
	{
		ArrayList<Route> routesArray = new ArrayList<Route>();
		JSONArray usersArray = getUsers();
		
		for(Object user : usersArray)
		{
			JSONObject userObj = (JSONObject) user;
			String person = (String) userObj.get("id");
			JSONArray savedRoutesArray = new JSONArray();
			
			//find the user's JSONObject who matches the user's ID
			if(person.equalsIgnoreCase(id))
			{
				//if favorite is true, return the saved routes data
				if(favorite)
				{
					savedRoutesArray = (JSONArray) userObj.get("savedRoutes");
				}
				
				//returns travel history data
				else
				{
					savedRoutesArray = (JSONArray) userObj.get("travelHistory");
				}
				
				//create a route object for every object in routesArray and add to the arraylist
				for(Object savedRoute : savedRoutesArray)
				{
					JSONObject routeObj = (JSONObject) savedRoute;
					String departure = (String) routeObj.get("departure");
					String arrival = (String) routeObj.get("arrival");
					Route route = new Route(departure, arrival);
					routesArray.add(route);
				}
			}
		}
		
		//show newest travel data first from travel history

		Collections.reverse(routesArray);
		return routesArray;
	}
	
	//save the chosen route in savedRoutes depending on the boolean, always save to travelHistory
	public void saveRoute(String id, String departure, String arrival) throws FileNotFoundException, IOException, ParseException
	{
		JSONArray usersArray = getUsers();
		String loc = new String("ovapp/src/data/users.json");
		
		for(Object user : usersArray)
		{
			JSONObject userObj = (JSONObject) user;
			String person = (String) userObj.get("id");
			JSONArray routes = new JSONArray();
			
			//only if save is true, save the route in savedRoutes
			if(person.equalsIgnoreCase(id))
			{
				routes = (JSONArray) userObj.get("savedRoutes");
				JSONObject route = new JSONObject();
				route.put("departure", departure);
				route.put("arrival", arrival);
				routes.add(route);
				
				Writer writer = new FileWriter(loc);
				usersArray.writeJSONString(writer);
				writer.close();
			}
		}
	}
	
	public void saveHistory(String id, String departure, String arrival) throws FileNotFoundException, IOException, ParseException
	{
		JSONArray usersArray = getUsers();
		String loc = new String("ovapp/src/data/users.json");
		
		for(Object user : usersArray)
		{
			JSONObject userObj = (JSONObject) user;
			String person = (String) userObj.get("id");
			JSONArray routes = new JSONArray();
			
			//only if save is true, save the route in savedRoutes
			if(person.equalsIgnoreCase(id))
			{
				routes = (JSONArray) userObj.get("travelHistory");
				JSONObject route = new JSONObject();
				route.put("departure", departure);
				route.put("arrival", arrival);
				routes.add(route);
				
				Writer writer = new FileWriter(loc);
				usersArray.writeJSONString(writer);
				writer.close();
			}
		}
	}
}
