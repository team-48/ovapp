package ovapp;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Stop 
{ 
	private String stop; //attribuut toevoegen
	private ArrayList<Busline> bussen = new ArrayList<>(); //arraylist van bussen aanmaken
	//private int tijd;

	public Stop(String stop)
	{
		this.stop = stop; //dit zorgt ervoor dat we stop kunnen aanroepen
	}

	public JSONObject toJSON()
    {                                               //hier maken me een json object of stoppen we objecten in een json object
		JSONObject object = new JSONObject();
		object.put("stop", stop);
		object.put("bussen", bussen);
		return object; 
	}
	
	public String getStop()
    { //hier maken we door middel van getters en setters onze private attributen aanroepbaar door andere classes
        return stop;
    }

    public void setStop(String stop)
    {
        this.stop = stop;
    }
    
    //public void getTijd(int tijd)

    public ArrayList<Busline> getBussen()
    {
        return bussen;
    }

    public void setBussen(ArrayList<Busline> bussen)
    {
        this.bussen = bussen;
    }

    public void addBussen(Busline bus)
    {
        this.bussen.add(bus);
    }
    
    public static Stop searchByStopName(JSONArray stops, String stopNaam)
    { // in deze methode vergelijken we de input met de stops in de lijst
        for (Object o : stops)
        { //loopen we door alle stops
            Stop stop = (Stop) o;
            if (stop.getStop().equalsIgnoreCase(stopNaam))
            {                                                            //if de stop equals de stop die we inputten return die stop
                return stop;
            }
        }
        return null;
    }
}