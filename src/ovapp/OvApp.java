package ovapp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.*;

//import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.json.simple.parser.ParseException;

public class OvApp
{
	JFrame frame, profileframe;
	JButton planRouteButton, savedRouteButton, profileButton, backButton;
	JCheckBox busCb, trainCb, tramCb, subwayCb, saveRoute;;
	JLabel userinfoLabel, departLabel, arriveLabel, recenttripsLabel, trajectinfoLabel, travelDataLabel, travelHistoryLabel, savedRouteLabel, 
		   inloglabel, inloglabel2, idlabel, nameLabel, lastnameLabel, streetLabel, placeLabel, firstLabel, timeLabel;
	JPanel userinfoPanel, inlogpanel, travelPanel, checkboxPanel, comboBoxPanel, panel4, panel5, panel6, panel7,
		   travelDataPanel, travelHistoryPanel, savedRoutePanel, profilePanel, profileboxPanel, firstPanel, backPanel;
	JComboBox<Object> departBox, arriveBox, timeBox;
	JComboBox<String> profileBox;
	ImageIcon logo; 
	Stop stop1, stop2, stop3, stop4, stop5;
	Busline bus; 
	Train train;
	TramLine tramLine;
	SubwayLine subwayLine;
	
	
	DataHandler manager = new DataHandler();
	ArrayList<Route> travelHistory = new ArrayList<Route>();
	ArrayList<Route> savedRoutes = new ArrayList<Route>();
	
	ArrayList<Busline> busses = new ArrayList<>();
	ArrayList<Train> trains = new ArrayList<>();
	ArrayList<TramLine> tram = new ArrayList<>();
	ArrayList<SubwayLine> subway = new ArrayList<>();
	
	Profile user;
	Profile user1 = new Profile("user1", "Peter" , "Pannenkoek" , "Hondenstraat 88" , "Deventer");
	Profile user2 = new Profile("user2", "Daan", "Janssen" , "Windmolen 14" , "Hilversum");
	Profile user3 = new Profile("user3", "Jan", "Vrij", "Dorpstraat 1", "Utrecht");
	
	public OvApp() throws ParseException
	{ 
		frame = new JFrame("Ov Applicatie");	
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setBackground(Color.white);								// grootte frame
		frame.setSize(650, 800);
		frame.setLocationRelativeTo(null);
		
//		logo = new ImageIcon(getClass().getClassLoader().getResource("OvApp_Icon.png"));
//		frame.setIconImage(logo.getImage());
		
		showprofilescreen(); profileframe.setVisible(false);
		getFrame();
		
		createBusRides();
		createTrain();
		createTram();
		createSubway();
		
		frame.setLayout(null);									// layout manager gebruiken 
		frame.setVisible(true);									// zichtbaar ? Ja	 	 
	}
	
	private void getFrame() throws ParseException
	{
		
		{																	//display first line of text
		    JLabel firstLineLabel = new JLabel("Waar wilt u heen?");
			Font Myfont = new Font(" Times ", Font.BOLD, 25);
			firstLineLabel.setFont(Myfont);
			firstLineLabel.setBounds(50, 20, 400, 30);
			frame.add(firstLineLabel);
		}
		
																			// add labels aankomst & vertrek
		{
			travelPanel = new JPanel();
			travelPanel.setBounds(50, 70, 600, 20);
			travelPanel.setLayout(new GridLayout(0, 3, 50, 0));
			travelPanel.setBackground(Color.white);
			
			departLabel = new JLabel("Vertreklocatie: ");
			departLabel.setFont(new Font("Times", Font.BOLD, 16));
			travelPanel.add(departLabel);
			
			arriveLabel = new JLabel("Aankomstlocatie: ");
			arriveLabel.setFont(new Font("Times", Font.BOLD, 16));
			travelPanel.add(arriveLabel);
			
			timeLabel = new JLabel("Tijd: ");
			timeLabel.setFont(new Font("Times", Font.BOLD, 16));
			travelPanel.add(timeLabel);
			
			frame.add(travelPanel);
		}	
			
		    																	// add checkboxen
		{    
			checkboxPanel = new JPanel();
			checkboxPanel.setLayout(new GridLayout(2,2));
			checkboxPanel.setBounds(47, 145, 220, 50);
			
			busCb = new JCheckBox("Bus");		busCb.setBackground(Color.white);  		busCb.setSelected(true);		checkboxPanel.add(busCb); 
			trainCb = new JCheckBox("Trein");	trainCb.setBackground(Color.white);		trainCb.setSelected(true); 		checkboxPanel.add(trainCb); 
			tramCb = new JCheckBox("Tram");		tramCb.setBackground(Color.white);		tramCb.setSelected(true);		checkboxPanel.add(tramCb); 
			subwayCb = new JCheckBox("Metro");	subwayCb.setBackground(Color.white);	subwayCb.setSelected(true);		checkboxPanel.add(subwayCb); 
			
			frame.add(checkboxPanel);
		}                                                                                                                                                        
		
		{ 															// comboboxen
			comboBoxPanel = new JPanel();
			comboBoxPanel.setBounds(50, 100, 394, 25);
			comboBoxPanel.setLayout(new GridLayout(0, 2, 40, 0));
			comboBoxPanel.setBackground(Color.white);
			
			String vertrek[] = {"Amersfoort Centraal", "Deventer", "Hilversum", "HU Amersfoort", "Utrecht Centraal"};
			departBox = new JComboBox<Object>(vertrek);
			departBox.setSelectedIndex(-1);
		//	AutoCompleteDecorator.decorate(departBox);
			comboBoxPanel.add(departBox);
			
			String aankomst[] = {"Amersfoort Centraal", "Deventer", "Hilversum", "HU Amersfoort", "Utrecht Centraal"};
			arriveBox = new JComboBox<Object>(aankomst);
			arriveBox.setSelectedIndex(-1);
		//	AutoCompleteDecorator.decorate(arriveBox);
			comboBoxPanel.add(arriveBox);

			LocalTime tijden [] = {
					LocalTime.of(10,00), LocalTime.of(10, 30), LocalTime.of(11,00),
					LocalTime.of(11, 30), LocalTime.of(11,00), LocalTime.of(12, 30) ,
					LocalTime.of(12,00), LocalTime.of(13, 30), LocalTime.of(13,00),
					LocalTime.of(14, 30), LocalTime.of(14,00), LocalTime.of(15, 30),
					LocalTime.of(15,00), LocalTime.of(16, 30), LocalTime.of(16,00),
					LocalTime.of(17, 30), LocalTime.of(17,00), LocalTime.of(18, 30) ,
					LocalTime.of(19,00), LocalTime.of(19, 30), LocalTime.of(20,00),
					LocalTime.of(20, 30), LocalTime.of(21,00), LocalTime.of(21, 30),
					LocalTime.of(22, 30)
			};
			timeBox = new JComboBox<Object>(tijden);
			timeBox.setSelectedIndex(-1);
			//AutoCompleteDecorator.decorate(timeBox);
			timeBox.setBounds(485, 100, 100, 25);
			frame.add(timeBox);
			frame.add(comboBoxPanel);	
		}
																		// add profilebutton to frame
		{
			 profilePanel = new JPanel();
			 profilePanel.setBounds(580, 10, 40, 40);
			 profilePanel.setBackground(Color.white);
			
			 profileButton = new JButton("Profiel");
			 profileButton.setSize(40, 40);
			 profileButton.addActionListener(new ActionListener() 
			 {
	
				public void actionPerformed(ActionEvent arg0) 
				{
					//inlogpanel.removeAll();
					//showprofilescreen();
					frame.setVisible(false);
					profileframe.setVisible(true);
				}
				 
			 });
			
			 profilePanel.add(profileButton);
			 frame.add(profilePanel);
		}														
															// add plan reis button with actionListener
	    {
			planRouteButton = new JButton("Plan Reis");
			planRouteButton.setBounds(270, 150, 100, 43);
			
			saveRoute = new JCheckBox("Save?");		saveRoute.setBackground(Color.white);  	saveRoute.setSelected(false);		frame.add(saveRoute);
			saveRoute.setBounds(480, 175, 70, 20);
			frame.add(planRouteButton);
																			// add actionlistener
			planRouteButton.addActionListener(new ActionListener()
			{
	
				public void actionPerformed(ActionEvent arg0)
				{
					travelPanel.removeAll();
					travelHistoryPanel.removeAll();
					savedRoutePanel.removeAll();
					travelDataPanel.removeAll();
					
					//show travel information
					showTravelData();
					
					//add to travel history
					if(!(departBox.getSelectedItem()==null) || !(arriveBox.getSelectedItem()==null))
					{
						try {
							manager.saveHistory(profileBox.getSelectedItem().toString(), departBox.getSelectedItem().toString(), arriveBox.getSelectedItem().toString());
						} catch (IOException | ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}

					//save selected route					
					if(saveRoute.isSelected())							
						try {
							manager.saveRoute(profileBox.getSelectedItem().toString(), departBox.getSelectedItem().toString(), arriveBox.getSelectedItem().toString());
							System.out.println("Route opgeslagen!");
						} catch (IOException | ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
					
					//show travel history
					try {
						travelHistory = manager.getRoutes(profileBox.getSelectedItem().toString(), false);
					} catch (IOException | ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//System.out.println(travelHistory);
					fillTravelHistory();
					
					try {
						savedRoutes = manager.getRoutes(profileBox.getSelectedItem().toString(), true);
					} catch (IOException | ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//System.out.println(savedRoutes);
					showSavedRoutes();
					
	
					travelHistoryPanel.setVisible(true);
				}
			});
		}
	    																		//travel data panel added
	    {
	    	panel7 = new JPanel();
	    	panel7.setLayout(new BoxLayout(panel7, BoxLayout.Y_AXIS));
	    	panel7.setBounds(50, 260, 530, 25);
	    	
	    	trajectinfoLabel = new JLabel();
	    	trajectinfoLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
	    	trajectinfoLabel.setText("Buslijn:          Vertrek:             Aankomst:");
	    	Font panel7Font = new Font(" Times ", Font.BOLD, 16);
	    	trajectinfoLabel.setFont(panel7Font);
	   
	    	panel7.add(trajectinfoLabel);
	    	frame.add(panel7);
	    	
	    	travelDataLabel = new JLabel();
	    	travelDataPanel = new JPanel();
	    	travelDataPanel.setBackground(Color.white);
	    	
	    	travelDataPanel.setLayout(new BoxLayout(travelDataPanel, BoxLayout.Y_AXIS));
	    	travelDataPanel.setBounds(50, 290, 530, 100);
	    	
	    	//add travel history components to the panel
	    	travelDataPanel.add(travelDataLabel);
	    	
	    	frame.add(travelDataPanel);
	    	
	    	panel7.setVisible(true);
	    	travelDataPanel.setVisible(false);
	    }
	    
	    																			//travel history panel added
	    {
	    	panel5 = new JPanel();
	    	panel5.setLayout(new BoxLayout(panel5, BoxLayout.Y_AXIS));
	    	panel5.setBounds(50, 395, 530, 25);
	    	
	    	recenttripsLabel = new JLabel();
	    	recenttripsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
	    	recenttripsLabel.setText("Uw recente reizen:");
	    	Font panel5Font = new Font(" Times ", Font.BOLD, 16);
	    	recenttripsLabel.setFont(panel5Font);
	    	panel5.add(recenttripsLabel);
	    	frame.add(panel5);
	    	
	    	travelHistoryLabel = new JLabel();
	    	travelHistoryPanel = new JPanel();
	    	travelHistoryPanel.setBackground(Color.white);
	    	
	    	//set BoxLayout to add items vertically under each other
	    	travelHistoryPanel.setLayout(new BoxLayout(travelHistoryPanel, BoxLayout.Y_AXIS));
	    	travelHistoryPanel.setBounds(50, 430, 530, 70);
	    	
	    	//add travel history components to the panel
	    	travelHistoryPanel.add(travelHistoryLabel);
	    	
	    	frame.add(travelHistoryPanel);
	    }	
	    																		//saved routes added
	    {
	    	panel6 = new JPanel();
	    	panel6.setLayout(new BoxLayout(panel6, BoxLayout.Y_AXIS));
	    	panel6.setBounds(50, 520, 530, 25);
	    	
	    	savedRouteLabel = new JLabel();
	    	savedRouteLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
	    	savedRouteLabel.setText("Uw opgeslagen reizen:");
	    	Font panel6Font = new Font(" Times ", Font.BOLD, 16);
	    	savedRouteLabel.setFont(panel6Font);
	    	panel6.add(savedRouteLabel);
	    	frame.add(panel6);
	    	
	    	//saved routes components added
	    	savedRoutePanel = new JPanel();
	    	savedRouteButton = new JButton();
	    	savedRoutePanel.setBackground(Color.white);
	    	savedRoutePanel.setLayout(new BoxLayout(savedRoutePanel, BoxLayout.Y_AXIS));
	    	savedRoutePanel.setBounds(50, 550, 530, 80);

	    	savedRoutePanel.add(savedRouteButton);
	    	savedRouteButton.setVisible(false);
	    	
	    	frame.add(savedRoutePanel);
	    }
	    
		{												// adding text "ingelogd als...."
			inlogpanel = new JPanel();
			inlogpanel.setBounds(450, 720, 200, 20);
			inlogpanel.setBackground(Color.white);
			
			inloglabel = new JLabel("Ingelogd als: GEBRUIKER1");
			Font labelFont = new Font(" Times ", Font.BOLD, 11);
			inloglabel.setFont(labelFont);
			
			inlogpanel.add(inloglabel);
			
			frame.add(inlogpanel);
		}
			
	}
	
    //fill travel history panel with data
    private void fillTravelHistory()
    {
    	travelHistoryPanel.removeAll();
    	//create labels when travel history list size is smaller than 3
    	if(travelHistory.size()<=3)
    	{
    		for(Route r : travelHistory) 
    		{
	    		travelHistoryLabel = new JLabel();
		    	travelHistoryLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		    	Font travelFont = new Font(" Times ", Font.BOLD, 12);
		    	travelHistoryLabel.setFont(travelFont);
		    	
	    		travelHistoryLabel.setText(r.toString());
	    		travelHistoryPanel.add(travelHistoryLabel);
    		}
    	}
    	else 
    	{
    		//create labels with travel history list with a maximum of 4 routes
    		for(int i = 0; i < 4; i++)
        	{
        		travelHistoryLabel = new JLabel();
    	    	travelHistoryLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    	    	Font travelFont = new Font(" Times ", Font.BOLD, 12);
    	    	travelHistoryLabel.setFont(travelFont);
    	    	
        		travelHistoryLabel.setText(travelHistory.get(i).toString());
        		travelHistoryPanel.add(travelHistoryLabel);
        	}
    	}
        travelHistoryPanel.revalidate();
        travelHistoryPanel.repaint();
    }
    
    private void showSavedRoutes()
    {
    	savedRoutePanel.removeAll();
    	
    	if(savedRoutes.size()<=2)
    	{	
	    	for(Route r : savedRoutes)
	    	{
	    		//create saved route buttons
	    		savedRouteButton = new JButton();
	    		savedRouteButton.setAlignmentX(Component.CENTER_ALIGNMENT);
	    		savedRouteButton.setText(r.toString());
	    		savedRouteButton.setBackground(Color.white);
	    		savedRouteButton.setBorderPainted(false);
	    	
	    		//put departure and arrival locations in the comboboxes when clicking the button
	    		savedRouteButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0)
					{
						departBox.setSelectedItem(r.getDeparture());
						arriveBox.setSelectedItem(r.getArrival());
					}
				});
	
	    		savedRoutePanel.add(savedRouteButton);
	    		savedRoutePanel.revalidate();
	    		savedRoutePanel.repaint();
	    	}
    	}
    	
    	else
    	{
    		for(int i = 0; i < 3; i++)
    		{
    			//create saved route buttons
    			int count = i;
	    		savedRouteButton = new JButton();
	    		savedRouteButton.setAlignmentX(Component.CENTER_ALIGNMENT);
	    		savedRouteButton.setText(savedRoutes.get(count).toString());
	    		savedRouteButton.setBackground(Color.white);
	    		savedRouteButton.setBorderPainted(false);
	    	
	    		//put departure and arrival locations in the comboboxes when clicking the button
	    		savedRouteButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0)
					{
						departBox.setSelectedItem(savedRoutes.get(count).getDeparture());
						arriveBox.setSelectedItem(savedRoutes.get(count).getArrival());
					}
				});
	
	    		savedRoutePanel.add(savedRouteButton);
	    		savedRoutePanel.revalidate();
	    		savedRoutePanel.repaint();
    		}
    	}
    }

	//create travelData
	private void showTravelData() {
		panel7.setVisible(true);
		travelDataPanel.setVisible(true);

		if (trainCb.isSelected()) {

			for (Train t : trains) {
				if (t.getVertrek().getStop().equals(departBox.getSelectedItem()) && t.getAankomst().getStop().equals(arriveBox.getSelectedItem())) {
					if (t.getTrainList().contains((LocalTime) timeBox.getSelectedItem())) {
						travelDataLabel = new JLabel();
						travelDataLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
						Font travelDataFont = new Font(" Times ", Font.BOLD, 12);
						travelDataLabel.setFont(travelDataFont);

						travelDataLabel.setText(t.toPrintTrainTime((LocalTime)timeBox.getSelectedItem()));
						travelDataPanel.add(travelDataLabel);
						travelDataPanel.revalidate();
						travelDataPanel.repaint();
					}
				}
			}
		}
		if (busCb.isSelected()) {

			for (Busline b : busses) {
				if (b.getVertrek().getStop().equals(departBox.getSelectedItem()) && b.getAankomst().getStop().equals(arriveBox.getSelectedItem())) {
					travelDataLabel = new JLabel();
					travelDataLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
					Font travelDataFont = new Font(" Times ", Font.BOLD, 12);
					travelDataLabel.setFont(travelDataFont);

					travelDataLabel.setText(b.toPrintBusTime((LocalTime)timeBox.getSelectedItem()));
					travelDataPanel.add(travelDataLabel);
					travelDataPanel.revalidate();
					travelDataPanel.repaint();
				}
			}
		}
		if (tramCb.isSelected()) {

			for (TramLine tl : tram) {
				if (tl.getVertrek().getStop().equals(departBox.getSelectedItem()) && tl.getAankomst().getStop().equals(arriveBox.getSelectedItem())) {
					travelDataLabel = new JLabel();
					travelDataLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
					Font travelDataFont = new Font(" Times ", Font.BOLD, 12);
					travelDataLabel.setFont(travelDataFont);

					travelDataLabel.setText(tl.toPrintTramLine((LocalTime)timeBox.getSelectedItem()));
					travelDataPanel.add(travelDataLabel);
					travelDataPanel.revalidate();
					travelDataPanel.repaint();
				}
			}
		}
		if (subwayCb.isSelected()) {
			for (SubwayLine s : subway) {
				if (s.getVertrek().getStop().equals(departBox.getSelectedItem()) && s.getAankomst().getStop().equals(arriveBox.getSelectedItem())) {
					travelDataLabel = new JLabel();
					travelDataLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
					Font travelDataFont = new Font(" Times ", Font.BOLD, 12);
					travelDataLabel.setFont(travelDataFont);

					travelDataLabel.setText(s.toPrintSubwayTime((LocalTime)timeBox.getSelectedItem()));
					travelDataPanel.add(travelDataLabel);
					travelDataPanel.revalidate();
					travelDataPanel.repaint();
				}
			}
		}
		if (subwayCb.isSelected() == false && tramCb.isSelected() == false && busCb.isSelected() == false && trainCb.isSelected() == false) {
			travelDataLabel = new JLabel();
			travelDataLabel.setText(null);
			travelDataPanel.revalidate();
			travelDataPanel.repaint();
		}
	}

	//create buslines
	private void createBusRides() {
		// locations added to stops
		stop1 = new Stop("Amersfoort Centraal");
		stop2 = new Stop("Deventer");
		stop3 = new Stop("Hilversum");
		stop4 = new Stop("HU Amersfoort");
		stop5 = new Stop("Utrecht Centraal");
		ArrayList<LocalTime>bustijdenLijn1 = new ArrayList<LocalTime>();
		ArrayList<LocalTime>bustijdenLijn2 = new ArrayList<LocalTime>();
		ArrayList<LocalTime>bustijdenLijn3 = new ArrayList<LocalTime>();
		ArrayList<LocalTime>bustijdenLijn4 = new ArrayList<LocalTime>();
		ArrayList<LocalTime>bustijdenLijn5 = new ArrayList<LocalTime>();
		ArrayList<LocalTime>bustijdenLijn6 = new ArrayList<LocalTime>();
		ArrayList<LocalTime>bustijdenLijn7 = new ArrayList<LocalTime>();
		ArrayList<LocalTime>bustijdenLijn8 = new ArrayList<LocalTime>();
		ArrayList<LocalTime>bustijdenLijn9 = new ArrayList<LocalTime>();
		ArrayList<LocalTime>bustijdenLijn10 = new ArrayList<LocalTime>();

		//Amersfoort Centraal
		bustijdenLijn1.add(LocalTime.of(10, 30, 19));
		bustijdenLijn1.add(LocalTime.of(1,2,2));
		bustijdenLijn1.add(LocalTime.of(1,2,2));
		bustijdenLijn1.add(LocalTime.of(1,2,2));
		bustijdenLijn1.add(LocalTime.of(1,2,2));
		bustijdenLijn1.add(LocalTime.of(1,2,2));
		bus = new Busline(1, stop1, stop2, bustijdenLijn1);
		busses.add(bus);

		bustijdenLijn2.add(LocalTime.of(1, 2, 3));
		bustijdenLijn2.add(LocalTime.of(1, 2, 3));
		bustijdenLijn2.add(LocalTime.of(1, 2, 3));
		bustijdenLijn2.add(LocalTime.of(1, 2, 3));
		bustijdenLijn2.add(LocalTime.of(1, 2, 3));
		bustijdenLijn2.add(LocalTime.of(1, 2, 3));
		bustijdenLijn2.add(LocalTime.of(1, 2, 3));
		bus = new Busline(2, stop1, stop2, bustijdenLijn2);
		busses.add(bus);

		//HU Amersfoort
		bustijdenLijn3.add(LocalTime.of(1, 1, 1));
		bustijdenLijn3.add(LocalTime.of(1, 1, 1));
		bustijdenLijn3.add(LocalTime.of(1, 1, 1));
		bustijdenLijn3.add(LocalTime.of(1, 1, 1));
		bustijdenLijn3.add(LocalTime.of(1, 1, 1));
		bustijdenLijn3.add(LocalTime.of(1, 1, 1));
		bus = new Busline(3, stop4, stop1, bustijdenLijn3);
		busses.add(bus);

		bustijdenLijn4.add(LocalTime.of(1,1,1));
		bustijdenLijn4.add(LocalTime.of(1,1,1));
		bustijdenLijn4.add(LocalTime.of(1,1,1));
		bustijdenLijn4.add(LocalTime.of(1,1,1));
		bustijdenLijn4.add(LocalTime.of(1,1,1));
		bustijdenLijn4.add(LocalTime.of(1,1,1));
		bus = new Busline(4, stop4, stop5, bustijdenLijn4);
		busses.add(bus);

//		//Utrecht Centraal
		bustijdenLijn5.add(LocalTime.of(1,1,1));
		bustijdenLijn5.add(LocalTime.of(1,1,1));
		bustijdenLijn5.add(LocalTime.of(1,1,1));
		bustijdenLijn5.add(LocalTime.of(1,1,1));
		bustijdenLijn5.add(LocalTime.of(1,1,1));
		bustijdenLijn5.add(LocalTime.of(1,1,1));
		bus = new Busline(5, stop5, stop1, bustijdenLijn5);
		busses.add(bus);

		bustijdenLijn6.add(LocalTime.of(1,1,1));
		bustijdenLijn6.add(LocalTime.of(1,1,1));
		bustijdenLijn6.add(LocalTime.of(1,1,1));
		bustijdenLijn6.add(LocalTime.of(1,1,1));
		bustijdenLijn6.add(LocalTime.of(1,1,1));
		bustijdenLijn6.add(LocalTime.of(1,1,1));
		bus = new Busline(6, stop5, stop4, bustijdenLijn6);
		busses.add(bus);

//		//Deventer
		bustijdenLijn7.add(LocalTime.of(1,11,1));
		bustijdenLijn7.add(LocalTime.of(1,1,1));
		bustijdenLijn7.add(LocalTime.of(1,11,1));
		bustijdenLijn7.add(LocalTime.of(1,1,1));
		bustijdenLijn7.add(LocalTime.of(1,11,1));
		bustijdenLijn7.add(LocalTime.of(1,1,1));
		bus = new Busline(7, stop2, stop1, bustijdenLijn7);
		busses.add(bus);

		bustijdenLijn8.add(LocalTime.of(1,1,1));
		bustijdenLijn8.add(LocalTime.of(1,1,1));
		bustijdenLijn8.add(LocalTime.of(1,1,1));
		bustijdenLijn8.add(LocalTime.of(1,1,1));
		bustijdenLijn8.add(LocalTime.of(1,1,1));
		bustijdenLijn8.add(LocalTime.of(1,1,1));
		bus = new Busline(8, stop2, stop3, bustijdenLijn8);
		busses.add(bus);

//		//Hilversum
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bustijdenLijn9.add(LocalTime.of(1,1,1));
		bus = new Busline(9, stop3, stop1, bustijdenLijn9);
		busses.add(bus);

		bustijdenLijn10.add(LocalTime.of(12,1,1));
		bustijdenLijn10.add(LocalTime.of(13,1,1));
		bustijdenLijn10.add(LocalTime.of(14,1,1));
		bustijdenLijn10.add(LocalTime.of(15,1,1));
		bustijdenLijn10.add(LocalTime.of(16,1,1));
		bustijdenLijn10.add(LocalTime.of(17,1,1));
		bustijdenLijn10.add(LocalTime.of(18,1,1));
		bustijdenLijn10.add(LocalTime.of(19,1,1));
		bustijdenLijn10.add(LocalTime.of(20,1,1));
		bustijdenLijn10.add(LocalTime.of(21,1,1));
		bustijdenLijn10.add(LocalTime.of(22,1,1));
		bustijdenLijn10.add(LocalTime.of(23,1,1));
		bus = new Busline(10, stop3, stop2, bustijdenLijn10);
		busses.add(bus);
	}

	//create train
	private void createTrain() {
		stop1 = new Stop("Amersfoort Centraal");
		stop2 = new Stop("Deventer");
		stop3 = new Stop("Hilversum");
		stop4 = new Stop("HU Amersfoort");
		stop5 = new Stop("Utrecht Centraal");

		ArrayList<LocalTime>trainList1 = new ArrayList<>();
		ArrayList<LocalTime>trainList2 = new ArrayList<>();
		ArrayList<LocalTime>trainList3 = new ArrayList<>();
		ArrayList<LocalTime>trainList4 = new ArrayList<>();
		ArrayList<LocalTime>trainList5 = new ArrayList<>();
		ArrayList<LocalTime>trainList6 = new ArrayList<>();
		ArrayList<LocalTime>trainList7 = new ArrayList<>();
		ArrayList<LocalTime>trainList8 = new ArrayList<>();
		ArrayList<LocalTime>trainList9 = new ArrayList<>();
		ArrayList<LocalTime>trainList10 = new ArrayList<>();

		//Amersfoort Centraal
		trainList1.add(LocalTime.of(10,30));
		trainList1.add(LocalTime.of(11,30));
		trainList1.add(LocalTime.of(12,00));
		trainList1.add(LocalTime.of(13,00));
		trainList1.add(LocalTime.of(12,00));
		trainList1.add(LocalTime.of(12,30));
		trainList1.add(LocalTime.of(14,30));
		trainList1.add(LocalTime.of(15,00));
		trainList1.add(LocalTime.of(16,00));
		trainList1.add(LocalTime.of(17,30));
		trainList1.add(LocalTime.of(18,00));
		trainList1.add(LocalTime.of(19,30));
		train = new Train(stop1, stop2, trainList1);
		trains.add(train);

		trainList2.add(LocalTime.of(10,30));
		trainList2.add(LocalTime.of(11,00));
		trainList2.add(LocalTime.of(12,30));
		trainList2.add(LocalTime.of(13,00));
		trainList2.add(LocalTime.of(14,30));
		trainList2.add(LocalTime.of(15,00));
		trainList2.add(LocalTime.of(16,30));
		trainList2.add(LocalTime.of(17,00));
		trainList2.add(LocalTime.of(18,30));
		trainList2.add(LocalTime.of(19,00));
		trainList2.add(LocalTime.of(20,30));
		trainList2.add(LocalTime.of(21,00));
		train = new Train(stop3, stop2, trainList2);
		trains.add(train);

		//Deventer
		trainList3.add(LocalTime.of(10,30));
		trainList3.add(LocalTime.of(11,00));
		trainList3.add(LocalTime.of(12,30));
		trainList3.add(LocalTime.of(13,00));
		trainList3.add(LocalTime.of(14,30));
		trainList3.add(LocalTime.of(15,00));
		trainList3.add(LocalTime.of(16,30));
		trainList3.add(LocalTime.of(17,00));
		trainList3.add(LocalTime.of(18,30));
		trainList3.add(LocalTime.of(19,00));
		trainList3.add(LocalTime.of(20,30));
		trainList3.add(LocalTime.of(21,00));
		train = new Train(stop2, stop3, trainList3);
		trains.add(train);

		trainList4.add(LocalTime.of(10,00));
		trainList4.add(LocalTime.of(11,30));
		trainList4.add(LocalTime.of(12,00));
		trainList4.add(LocalTime.of(13,30));
		trainList4.add(LocalTime.of(14,00));
		trainList4.add(LocalTime.of(15,30));
		trainList4.add(LocalTime.of(16,00));
		trainList4.add(LocalTime.of(17,30));
		trainList4.add(LocalTime.of(18,00));
		trainList4.add(LocalTime.of(19,30));
		trainList4.add(LocalTime.of(20,00));
		trainList4.add(LocalTime.of(21,30));
		train = new Train(stop2, stop4, trainList4);
		trains.add(train);

		//Hilversum
		trainList5.add(LocalTime.of(10,00));
		trainList5.add(LocalTime.of(11,00));
		trainList5.add(LocalTime.of(12,00));
		trainList5.add(LocalTime.of(13,00));
		trainList5.add(LocalTime.of(14,00));
		trainList5.add(LocalTime.of(15,00));
		trainList5.add(LocalTime.of(16,00));
		trainList5.add(LocalTime.of(17,00));
		trainList5.add(LocalTime.of(18,00));
		trainList5.add(LocalTime.of(19,00));
		trainList5.add(LocalTime.of(20,00));
		trainList5.add(LocalTime.of(21,00));
		train = new Train(stop3, stop1, trainList5);
		trains.add(train);

		trainList6.add(LocalTime.of(10,30));
		trainList6.add(LocalTime.of(11,30));
		trainList6.add(LocalTime.of(12,30));
		trainList6.add(LocalTime.of(13,30));
		trainList6.add(LocalTime.of(14,30));
		trainList6.add(LocalTime.of(15,30));
		trainList6.add(LocalTime.of(16,30));
		trainList6.add(LocalTime.of(17,30));
		trainList6.add(LocalTime.of(18,30));
		trainList6.add(LocalTime.of(19,30));
		trainList6.add(LocalTime.of(20,30));
		trainList6.add(LocalTime.of(21,30));
		train = new Train(stop3, stop5, trainList6);
		trains.add(train);

		//Hu Amersfoort
		trainList7.add(LocalTime.of(10,30));
		trainList7.add(LocalTime.of(20,00));
		train = new Train(stop4, stop5, trainList7);
		trains.add(train);

		trainList8.add(LocalTime.of(10,00));
		trainList8.add(LocalTime.of(11,30));
		trainList8.add(LocalTime.of(20,00));
		trainList8.add(LocalTime.of(21,30));
		train = new Train(stop4, stop1, trainList8);
		trains.add(train);

		//Utrecht Centraal
		trainList9.add(LocalTime.of(19,00));
		trainList9.add(LocalTime.of(19,30));
		trainList9.add(LocalTime.of(20,00));
		trainList9.add(LocalTime.of(20,30));
		train = new Train(stop5, stop2, trainList9);
		trains.add(train);

		trainList10.add(LocalTime.of(10,00));
		trainList10.add(LocalTime.of(11,30));
		trainList10.add(LocalTime.of(12,00));
		trainList10.add(LocalTime.of(13,30));
		trainList10.add(LocalTime.of(14,00));
		trainList10.add(LocalTime.of(15,30));
		trainList10.add(LocalTime.of(16,00));
		trainList10.add(LocalTime.of(17,30));
		trainList10.add(LocalTime.of(18,00));
		trainList10.add(LocalTime.of(19,30));
		trainList10.add(LocalTime.of(20,00));
		trainList10.add(LocalTime.of(20,30));
		train = new Train(stop5, stop1, trainList10);
		trains.add(train);
	}

	//create tramLine
	private void createTram() {
		stop1 = new Stop ("Amersfoort Centraal");
		stop2 = new Stop("Deventer");
		stop3 = new Stop("Hilversum");
		stop4 = new Stop("HU Amersfoort");
		stop5 = new Stop("Utrecht Centraal");

		ArrayList<LocalTime>tramList1 = new ArrayList<>();
		ArrayList<LocalTime>tramList2 = new ArrayList<>();
		ArrayList<LocalTime>tramList3 = new ArrayList<>();
		ArrayList<LocalTime>tramList4 = new ArrayList<>();
		ArrayList<LocalTime>tramList5 = new ArrayList<>();
		ArrayList<LocalTime>tramList6 = new ArrayList<>();
		ArrayList<LocalTime>tramList7 = new ArrayList<>();
		ArrayList<LocalTime>tramList8 = new ArrayList<>();
		ArrayList<LocalTime>tramList9 = new ArrayList<>();
		ArrayList<LocalTime>tramList10 = new ArrayList<>();

		//Amersfoort Centraal
		tramList1.add(LocalTime.of(11,1,1));
		tramList1.add(LocalTime.of(12,1,1));
		tramList1.add(LocalTime.of(13,1,1));
		tramList1.add(LocalTime.of(14,1,1));
		tramList1.add(LocalTime.of(15,1,1));
		tramList1.add(LocalTime.of(16,1,1));
		tramList1.add(LocalTime.of(17,1,1));
		tramList1.add(LocalTime.of(18,1,1));
		tramList1.add(LocalTime.of(19,1,1));
		tramList1.add(LocalTime.of(20,1,1));
		tramList1.add(LocalTime.of(21,1,1));
		tramList1.add(LocalTime.of(22,1,1));
		tramLine = new TramLine(2, stop1, stop2, tramList1);
		tram.add(tramLine);

		tramList2.add(LocalTime.of(1,1,1));
		tramList2.add(LocalTime.of(1,1,1));
		tramList2.add(LocalTime.of(1,1,1));
		tramList2.add(LocalTime.of(1,1,1));
		tramList2.add(LocalTime.of(1,1,1));
		tramList2.add(LocalTime.of(1,1,1));
		tramList2.add(LocalTime.of(1,1,1));
		tramList2.add(LocalTime.of(1,1,1));
		tramList2.add(LocalTime.of(1,1,1));
		tramList2.add(LocalTime.of(1,1,1));
		tramList2.add(LocalTime.of(1,1,1));
		tramList2.add(LocalTime.of(1,1,1));
		tramLine = new TramLine(2, stop1, stop3, tramList2);
		tram.add(tramLine);

		//Deventer
		tramList3.add(LocalTime.of(1,1,1));
		tramList3.add(LocalTime.of(1,1,1));
		tramList3.add(LocalTime.of(1,1,1));
		tramList3.add(LocalTime.of(1,1,1));
		tramList3.add(LocalTime.of(1,1,1));
		tramList3.add(LocalTime.of(1,1,1));
		tramList3.add(LocalTime.of(1,1,1));
		tramList3.add(LocalTime.of(1,1,1));
		tramList3.add(LocalTime.of(1,1,1));
		tramList3.add(LocalTime.of(1,1,1));
		tramList3.add(LocalTime.of(1,1,1));
		tramList3.add(LocalTime.of(1,1,1));
		tramLine = new TramLine(3, stop2, stop3, tramList3);
		tram.add(tramLine);

		tramList4.add(LocalTime.of(1,1,1));
		tramList4.add(LocalTime.of(1,1,1));
		tramList4.add(LocalTime.of(1,1,1));
		tramList4.add(LocalTime.of(1,1,1));
		tramList4.add(LocalTime.of(1,1,1));
		tramList4.add(LocalTime.of(1,1,1));
		tramList4.add(LocalTime.of(1,1,1));
		tramList4.add(LocalTime.of(1,1,1));
		tramList4.add(LocalTime.of(1,1,1));
		tramList4.add(LocalTime.of(1,1,1));
		tramList4.add(LocalTime.of(1,1,1));
		tramList4.add(LocalTime.of(1,1,1));
		tramLine = new TramLine(4, stop2, stop1, tramList4);
		tram.add(tramLine);

		//Hilversum
		tramList5.add(LocalTime.of(1,1,1));
		tramList5.add(LocalTime.of(1,1,1));
		tramList5.add(LocalTime.of(1,1,1));
		tramList5.add(LocalTime.of(1,1,1));
		tramList5.add(LocalTime.of(1,1,1));
		tramList5.add(LocalTime.of(1,1,1));
		tramList5.add(LocalTime.of(1,1,1));
		tramList5.add(LocalTime.of(1,1,1));
		tramList5.add(LocalTime.of(1,1,1));
		tramList5.add(LocalTime.of(1,1,1));
		tramList5.add(LocalTime.of(1,1,1));
		tramList5.add(LocalTime.of(1,1,1));
		tramLine = new TramLine(5, stop3, stop2, tramList5);
		tram.add(tramLine);

		tramList6.add(LocalTime.of(1,1,1));
		tramList6.add(LocalTime.of(1,1,1));
		tramList6.add(LocalTime.of(1,1,1));
		tramList6.add(LocalTime.of(1,1,1));
		tramList6.add(LocalTime.of(1,1,1));
		tramList6.add(LocalTime.of(1,1,1));
		tramList6.add(LocalTime.of(1,1,1));
		tramList6.add(LocalTime.of(1,1,1));
		tramList6.add(LocalTime.of(1,1,1));
		tramList6.add(LocalTime.of(1,1,1));
		tramList6.add(LocalTime.of(1,1,1));
		tramList6.add(LocalTime.of(1,1,1));
		tramLine = new TramLine(6, stop3, stop1, tramList6);
		tram.add(tramLine);

		//Hu Amersfoort
		tramList7.add(LocalTime.of(1,1,1));
		tramList7.add(LocalTime.of(1,1,1));
		tramList7.add(LocalTime.of(1,1,1));
		tramList7.add(LocalTime.of(1,1,1));
		tramList7.add(LocalTime.of(1,1,1));
		tramList7.add(LocalTime.of(1,1,1));
		tramList7.add(LocalTime.of(1,1,1));
		tramList7.add(LocalTime.of(1,1,1));
		tramList7.add(LocalTime.of(1,1,1));
		tramList7.add(LocalTime.of(1,1,1));
		tramList7.add(LocalTime.of(1,1,1));
		tramList7.add(LocalTime.of(1,1,1));
		tramLine = new TramLine(7, stop4, stop5, tramList7);
		tram.add(tramLine);

		tramList8.add(LocalTime.of(1,1,1));
		tramList8.add(LocalTime.of(1,1,1));
		tramList8.add(LocalTime.of(1,1,1));
		tramList8.add(LocalTime.of(1,1,1));
		tramList8.add(LocalTime.of(1,1,1));
		tramList8.add(LocalTime.of(1,1,1));
		tramList8.add(LocalTime.of(1,1,1));
		tramList8.add(LocalTime.of(1,1,1));
		tramList8.add(LocalTime.of(1,1,1));
		tramList8.add(LocalTime.of(1,1,1));
		tramList8.add(LocalTime.of(1,1,1));
		tramList8.add(LocalTime.of(1,1,1));
		tramLine = new TramLine(8, stop4, stop1, tramList8);
		tram.add(tramLine);

		//Utrecht Centraal
		tramList9.add(LocalTime.of(1,1,1));
		tramList9.add(LocalTime.of(1,1,1));
		tramList9.add(LocalTime.of(1,1,1));
		tramList9.add(LocalTime.of(1,1,1));
		tramList9.add(LocalTime.of(1,1,1));
		tramList9.add(LocalTime.of(1,1,1));
		tramList9.add(LocalTime.of(1,1,1));
		tramList9.add(LocalTime.of(1,1,1));
		tramList9.add(LocalTime.of(1,1,1));
		tramList9.add(LocalTime.of(1,1,1));
		tramList9.add(LocalTime.of(1,1,1));
		tramList9.add(LocalTime.of(1,1,1));
		tramLine = new TramLine(9, stop5, stop2, tramList9);
		tram.add(tramLine);

		tramList10.add(LocalTime.of(1,1,1));
		tramList10.add(LocalTime.of(1,1,1));
		tramList10.add(LocalTime.of(1,1,1));
		tramList10.add(LocalTime.of(1,1,1));
		tramList10.add(LocalTime.of(1,1,1));
		tramList10.add(LocalTime.of(1,1,1));
		tramList10.add(LocalTime.of(1,1,1));
		tramList10.add(LocalTime.of(1,1,1));
		tramList10.add(LocalTime.of(1,1,1));
		tramList10.add(LocalTime.of(1,1,1));
		tramList10.add(LocalTime.of(1,1,1));
		tramList10.add(LocalTime.of(1,1,1));
		tramLine = new TramLine(10, stop5, stop1, tramList10);
		tram.add(tramLine);

	}

	//create subwayLine
	private void createSubway() {
		stop1 = new Stop ("Amersfoort Centraal");
		stop2 = new Stop("Deventer");
		stop3 = new Stop("Hilversum");
		stop4 = new Stop("HU Amersfoort");
		stop5 = new Stop("Utrecht Centraal");

		ArrayList<LocalTime>subwayList1 = new ArrayList<>();
		ArrayList<LocalTime>subwayList2 = new ArrayList<>();
		ArrayList<LocalTime>subwayList3 = new ArrayList<>();
		ArrayList<LocalTime>subwayList4 = new ArrayList<>();
		ArrayList<LocalTime>subwayList5 = new ArrayList<>();
		ArrayList<LocalTime>subwayList6 = new ArrayList<>();
		ArrayList<LocalTime>subwayList7 = new ArrayList<>();
		ArrayList<LocalTime>subwayList8 = new ArrayList<>();
		ArrayList<LocalTime>subwayList9 = new ArrayList<>();
		ArrayList<LocalTime>subwayList10 = new ArrayList<>();

		//Amersfoort Centraal
		subwayList1.add(LocalTime.of(10,1,1));
		subwayList1.add(LocalTime.of(11,1,1));
		subwayList1.add(LocalTime.of(12,1,1));
		subwayList1.add(LocalTime.of(13,1,1));
		subwayList1.add(LocalTime.of(14,1,1));
		subwayList1.add(LocalTime.of(15,1,1));
		subwayList1.add(LocalTime.of(16,1,1));
		subwayList1.add(LocalTime.of(17,1,1));
		subwayList1.add(LocalTime.of(19,1,1));
		subwayLine = new SubwayLine (1,stop1, stop2, subwayList1);
		subway.add(subwayLine);

		subwayList2.add(LocalTime.of(1,1,1));
		subwayList2.add(LocalTime.of(1,1,1));
		subwayList2.add(LocalTime.of(1,1,1));
		subwayList2.add(LocalTime.of(1,1,1));
		subwayList2.add(LocalTime.of(1,1,1));
		subwayList2.add(LocalTime.of(1,1,1));
		subwayList2.add(LocalTime.of(1,1,1));
		subwayList2.add(LocalTime.of(1,1,1));
		subwayList2.add(LocalTime.of(1,1,1));
		subwayList2.add(LocalTime.of(1,1,1));
		subwayList2.add(LocalTime.of(1,1,1));
		subwayList2.add(LocalTime.of(1,1,1));
		subwayLine = new SubwayLine(2, stop1, stop3, subwayList2);
		subway.add(subwayLine);

		//Deventer
		subwayList3.add(LocalTime.of(1,1,1));
		subwayList3.add(LocalTime.of(1,1,1));
		subwayList3.add(LocalTime.of(1,1,1));
		subwayList3.add(LocalTime.of(1,1,1));
		subwayList3.add(LocalTime.of(1,1,1));
		subwayList3.add(LocalTime.of(1,1,1));
		subwayList3.add(LocalTime.of(1,1,1));
		subwayList3.add(LocalTime.of(1,1,1));
		subwayList3.add(LocalTime.of(1,1,1));
		subwayList3.add(LocalTime.of(1,1,1));
		subwayList3.add(LocalTime.of(1,1,1));
		subwayList3.add(LocalTime.of(1,1,1));
		subwayLine = new SubwayLine(3, stop2, stop4, subwayList3);
		subway.add(subwayLine);

		subwayList4.add(LocalTime.of(1,1,1));
		subwayList4.add(LocalTime.of(1,1,1));
		subwayList4.add(LocalTime.of(1,1,1));
		subwayList4.add(LocalTime.of(1,1,1));
		subwayList4.add(LocalTime.of(1,1,1));
		subwayList4.add(LocalTime.of(1,1,1));
		subwayList4.add(LocalTime.of(1,1,1));
		subwayList4.add(LocalTime.of(1,1,1));
		subwayList4.add(LocalTime.of(1,1,1));
		subwayList4.add(LocalTime.of(1,1,1));
		subwayList4.add(LocalTime.of(1,1,1));
		subwayList4.add(LocalTime.of(1,1,1));
		subwayLine = new SubwayLine(4, stop2, stop3, subwayList4);
		subway.add(subwayLine);

		//Hilversum
		subwayList5.add(LocalTime.of(1,1,1));
		subwayList5.add(LocalTime.of(1,1,1));
		subwayList5.add(LocalTime.of(1,1,1));
		subwayList5.add(LocalTime.of(1,1,1));
		subwayList5.add(LocalTime.of(1,1,1));
		subwayList5.add(LocalTime.of(1,1,1));
		subwayList5.add(LocalTime.of(1,1,1));
		subwayList5.add(LocalTime.of(1,1,1));
		subwayList5.add(LocalTime.of(1,1,1));
		subwayList5.add(LocalTime.of(1,1,1));
		subwayList5.add(LocalTime.of(1,1,1));
		subwayList5.add(LocalTime.of(1,1,1));
		subwayLine = new SubwayLine(5, stop3, stop2, subwayList5);
		subway.add(subwayLine);

		subwayList6.add(LocalTime.of(1,1,1));
		subwayList6.add(LocalTime.of(1,1,1));
		subwayList6.add(LocalTime.of(1,1,1));
		subwayList6.add(LocalTime.of(1,1,1));
		subwayList6.add(LocalTime.of(1,1,1));
		subwayList6.add(LocalTime.of(1,1,1));
		subwayList6.add(LocalTime.of(1,1,1));
		subwayList6.add(LocalTime.of(1,1,1));
		subwayList6.add(LocalTime.of(1,1,1));
		subwayList6.add(LocalTime.of(1,1,1));
		subwayList6.add(LocalTime.of(1,1,1));
		subwayList6.add(LocalTime.of(1,1,1));
		subwayLine = new SubwayLine(6, stop3, stop1, subwayList6);
		subway.add(subwayLine);

		//Hu Amersfoort
		subwayList7.add(LocalTime.of(1,1,1));
		subwayList7.add(LocalTime.of(1,1,1));
		subwayList7.add(LocalTime.of(1,1,1));
		subwayList7.add(LocalTime.of(1,1,1));
		subwayList7.add(LocalTime.of(1,1,1));
		subwayList7.add(LocalTime.of(1,1,1));
		subwayList7.add(LocalTime.of(1,1,1));
		subwayList7.add(LocalTime.of(1,1,1));
		subwayList7.add(LocalTime.of(1,1,1));
		subwayList7.add(LocalTime.of(1,1,1));
		subwayList7.add(LocalTime.of(1,1,1));
		subwayList7.add(LocalTime.of(1,1,1));
		subwayLine = new SubwayLine(7, stop4, stop1, subwayList7);
		subway.add(subwayLine);

		subwayList8.add(LocalTime.of(1,1,1));
		subwayList8.add(LocalTime.of(1,1,1));
		subwayList8.add(LocalTime.of(1,1,1));
		subwayList8.add(LocalTime.of(1,1,1));
		subwayList8.add(LocalTime.of(1,1,1));
		subwayList8.add(LocalTime.of(1,1,1));
		subwayList8.add(LocalTime.of(1,1,1));
		subwayList8.add(LocalTime.of(1,1,1));
		subwayList8.add(LocalTime.of(1,1,1));
		subwayList8.add(LocalTime.of(1,1,1));
		subwayList8.add(LocalTime.of(1,1,1));
		subwayList8.add(LocalTime.of(1,1,1));
		subwayLine = new SubwayLine(8, stop4, stop1, subwayList8);
		subway.add(subwayLine);

		//Utrecht Centraal
		subwayList9.add(LocalTime.of(1,1,1));
		subwayList9.add(LocalTime.of(1,1,1));
		subwayList9.add(LocalTime.of(1,1,1));
		subwayList9.add(LocalTime.of(1,1,1));
		subwayList9.add(LocalTime.of(1,1,1));
		subwayList9.add(LocalTime.of(1,1,1));
		subwayList9.add(LocalTime.of(1,1,1));
		subwayList9.add(LocalTime.of(1,1,1));
		subwayList9.add(LocalTime.of(1,1,1));
		subwayList9.add(LocalTime.of(1,1,1));
		subwayList9.add(LocalTime.of(1,1,1));
		subwayList9.add(LocalTime.of(1,1,1));
		subwayLine = new SubwayLine(9, stop5, stop1, subwayList9);
		subway.add(subwayLine);

		subwayList10.add(LocalTime.of(1,1,1));
		subwayList10.add(LocalTime.of(1,1,1));
		subwayList10.add(LocalTime.of(1,1,1));
		subwayList10.add(LocalTime.of(1,1,1));
		subwayList10.add(LocalTime.of(1,1,1));
		subwayList10.add(LocalTime.of(1,1,1));
		subwayList10.add(LocalTime.of(1,1,1));
		subwayList10.add(LocalTime.of(1,1,1));
		subwayList10.add(LocalTime.of(1,1,1));
		subwayList10.add(LocalTime.of(1,1,1));
		subwayList10.add(LocalTime.of(1,1,1));
		subwayList10.add(LocalTime.of(1,1,1));
		subwayLine = new SubwayLine(10, stop5, stop2, subwayList10);
		subway.add(subwayLine);

	}

	private void showprofilescreen()
	{														// frame information
		profileframe = new JFrame("Profiel");
		profileframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		profileframe.getContentPane().setBackground(Color.white);
		profileframe.setSize(650, 800);
		profileframe.setLocationRelativeTo(null);
		
//		logo = new ImageIcon(getClass().getClassLoader().getResource("OvApp_Icon.png"));
//		profileframe.setIconImage(logo.getImage());
		
		{															// label/panel with userinfo
			firstPanel = new JPanel();
			firstPanel.setBounds(50, 0, 530, 50);
			firstPanel.setBackground(Color.white);
	
			firstLabel = new JLabel("Ingelogd als: GEBRUIKER 1");
			firstLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
			Font beginlabelFont = new Font(" Times ", Font.BOLD, 20);
			firstLabel.setFont(beginlabelFont);

			firstPanel.add(firstLabel);
		
			profileframe.add(firstPanel);	
		}	
																	// add combobox with different users in it
		{
			profileboxPanel = new JPanel();
			profileboxPanel.setBounds(50, 50, 530, 50);
			profileboxPanel.setBackground(Color.white);
			String profiles[] = {user1.getID(), user2.getID(), user3.getID()}; 
			profileBox = new JComboBox<String>(profiles);
			
			profileBox.addItemListener(new ItemListener()
			{
				public void itemStateChanged(ItemEvent e) 					// add itemlistener to the combobox
				{
					if(e.getStateChange()==ItemEvent.SELECTED)
					{	
						user = new Profile();
						try {
							user = manager.getProfile(profileBox.getSelectedItem().toString());
							System.out.println(user);
						} catch (IOException | ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						{
							//load profile data based on the combobox of the users
							firstLabel.setText("Ingelogd als: " + user.getID());
							
							inlogpanel.removeAll();
							inloglabel2 = new JLabel();
							inloglabel2.setText("Ingelogd als: " + user.getID());
							Font labelFont = new Font(" Times ", Font.BOLD, 10);
							inloglabel2.setFont(labelFont);
							inlogpanel.add(inloglabel2);
							
							idlabel.setText("Gebruiker:                       " + user.getID()); 							
							nameLabel.setText("Naam:                                         " + user.getFirstName()); 	
							lastnameLabel.setText("Achternaam:                        " + user.getLastName());		
							streetLabel.setText("Adres:                         " + user.getAdress());					
							placeLabel.setText("Woonplaats:                     " + user.getCity());	
						}
					}
					
				}
				
			});
			
			profileboxPanel.add(profileBox);
			profileframe.add(profileboxPanel);
		}
																		// adding backpanel with a backbutton
		{
			backPanel = new JPanel();
			backPanel.setBounds(220, 600, 200, 50);
			backPanel.setLayout(new GridLayout(0, 1, 10, 0));
			//backPanel.setBackground(Color.gray);
			
			backButton = new JButton("Back");
			backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
			backButton.addActionListener(new ActionListener() 
			{

				public void actionPerformed(ActionEvent e) 
				{
					//updateLabel();
					profileframe.setVisible(false);
					frame.setVisible(true);
					inlogpanel.revalidate();
					inlogpanel.repaint();
				}
				
			});
			
			backPanel.add(backButton);
			profileframe.add(backPanel);
		}
														// adding label and panel of "userinfo" and display userinfo
 		{
			userinfoLabel = new JLabel("Info Gebruiker: ");
			userinfoLabel.setBounds(260, 150, 200, 20);
			Font fontt= new Font(" Times ", Font.BOLD, 14);
			userinfoLabel.setFont(fontt);

			profileframe.add(userinfoLabel);
			
			userinfoPanel = new JPanel();
			userinfoPanel.setBounds(170, 200, 300, 200);
			userinfoPanel.setBackground(Color.white);
																			// make labels for userinfo
			idlabel = new JLabel(); 			idlabel.setFont(fontt);    
			nameLabel = new JLabel(); 			nameLabel.setFont(fontt);
			lastnameLabel = new JLabel(); 	lastnameLabel.setFont(fontt);
			streetLabel = new JLabel(); 		streetLabel.setFont(fontt);
			placeLabel = new JLabel(); 		placeLabel.setFont(fontt);
																				// display userinfo
			idlabel.setText("Gebruiker:                       " + user1.getID()); 							
			nameLabel.setText("Naam:                                         " + user1.getFirstName()); 	
			lastnameLabel.setText("Achternaam:                  " + user1.getLastName());					
			streetLabel.setText("Adres:                      " + user1.getAdress());							
			placeLabel.setText("Woonplaats:                       " + user1.getCity());	
			
			userinfoPanel.add(idlabel);                						// adding components to panel
			userinfoPanel.add(nameLabel);              
			userinfoPanel.add(lastnameLabel);
			userinfoPanel.add(streetLabel);        
			userinfoPanel.add(placeLabel);            
																			// adding panel to frame
			profileframe.add(userinfoPanel);
		}
		
			profileframe.setLayout(null);
			profileframe.setVisible(true);
	}
	
}