package ovapp;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import static java.lang.Math.abs;

public class Train {
    private Stop vertrekLocatie;
    private Stop aankomstLocatie;
    private ArrayList<LocalTime> trainList;

    public Train( Stop vertrekLocatie, Stop aankomstLocatie, ArrayList<LocalTime> trainList) { //Constructor/aanroepen
        this.vertrekLocatie = vertrekLocatie;
        this.aankomstLocatie = aankomstLocatie;
        this.trainList = trainList;
    }

    public Stop getVertrek() {
        return vertrekLocatie;
    }

    public Stop getAankomst() {
        return aankomstLocatie;
    }

    public ArrayList<LocalTime> getTrainList() {return trainList;}

    public String toPrintTrainTime(LocalTime departureTime) {
        return String.format("Train %s %s %s\n", vertrekLocatie.getStop(), aankomstLocatie.getStop(), departureTime);
    }

}
