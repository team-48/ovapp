package ovapp;

public class Route 
{
	
	private String departure;
	private String arrival;
	
	public Route()
	{
		this(null, null);
	}
	
	public Route(String departure, String arrival) 
	{
		this.departure = departure;
		this.arrival = arrival;
	}
	
	public String getDeparture()
	{
		return departure;
	}
	
	public void setDeparture(String departure)
	{
		this.departure = departure;
	}
	
	public String getArrival()
	{
		return arrival;
	}
	
	public void getArrival(String arrival)
	{
		this.arrival = arrival;
	}
	
	public String toString()
	{
		return departure + "-" + arrival;
	}
}
