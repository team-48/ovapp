package ovapp;

import java.time.LocalTime;
import java.util.ArrayList;

public class TramLine {

    private int tramLine; // atributen
    private Stop vertrekLocatie;
    private Stop aankomstLocatie;
    private ArrayList<LocalTime>tramList;


    public TramLine(int tramLine, Stop vertrekLocatie, Stop aankomstLocatie, ArrayList <LocalTime> tramList) { //dit zorgt ervoor dat we buslijn en route kunnen gebruiken/aanroepen
        this.tramLine = tramLine;
        this.vertrekLocatie = vertrekLocatie;
        this.aankomstLocatie = aankomstLocatie;
        this.tramList = tramList;
    }
    public int getTramLine() {
        return tramLine;
    }

    public Stop getVertrek() {
        return vertrekLocatie;
    }

    public Stop getAankomst() {
        return aankomstLocatie;
    }
    public ArrayList getTramlist() {
        return tramList;
    }

    public String toPrintTramLine(LocalTime departureTime) {
        return String.format("Tramlijn %d %s %s %s\n", tramLine, vertrekLocatie.getStop(), aankomstLocatie.getStop(), departureTime);
    }

}
