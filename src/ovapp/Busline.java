package ovapp;

import java.time.LocalTime;
import java.util.ArrayList;

public class Busline {

	private int busline; // atributen
	private Stop vertrekLocatie;
	private Stop aankomstLocatie;
	private ArrayList<LocalTime> tijdenLijst;

	public Busline(int busline, Stop vertrekLocatie, Stop aankomstLocatie, ArrayList<LocalTime> tijdenLijst){
		this.busline = busline;
		this.vertrekLocatie = vertrekLocatie;
		this.aankomstLocatie = aankomstLocatie;
		this.tijdenLijst = tijdenLijst;
	}
	public int getBusline() {
		return busline;
	}

	public Stop getVertrek() {
		return vertrekLocatie;
	}

	public Stop getAankomst() {
		return aankomstLocatie;
	}

	//public LocalTime getTijdenLijst(){
	//	return tijdenLijst;
	//}

	public String toPrintBusTime(LocalTime departureTime) {
		return String.format("buslijn %d %s %s %s\n", busline, vertrekLocatie.getStop(), aankomstLocatie.getStop(), departureTime);
	}

}
