package ovapp;

import java.util.ArrayList;

public class Profile 
{
	
	private String id;
	private String firstName;
	private String lastName;
	private String adress;
	private String city;
	private ArrayList<Route> travelHistory = new ArrayList<Route>();
	private ArrayList<Route> savedRoutes = new ArrayList<Route>();
	
	public Profile()
	{
		this(null, null, null, null, null);
	};
	
	public Profile(String id)
	{
		this(id, null, null, null, null);
	}
	
	public Profile(String id, String firstName, String lastName, 
			   String adress, String city)
	{
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.adress = adress;
		this.city = city;
	}
	
	public String getID()
	{
		return id;
	}
	
	public void setID(String id)
	{
		this.id = id;
	}
	
	public String getFirstName()
	{
		return firstName;
	}
	
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	public String getAdress()
	{
		return adress;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public void setCity(String city)
	{
		this.city = city;
	}
	
	public ArrayList<Route> getTravelHistory()
	{
		return travelHistory;
	}
	
	public void setTravelHistory(ArrayList<Route> travelHistory)
	{
		this.travelHistory = travelHistory;
	}
	
	public void addTravelHistory(Route route)
	{
		this.travelHistory.add(route);
	}
	
	public ArrayList<Route> getSavedRoutes()
	{
		return savedRoutes;
	}
	
	public void setSavedRoutes(ArrayList<Route> savedRoutes)
	{
		this.savedRoutes = savedRoutes;
	}
	
	public void addSavedRoutes(Route route)
	{
		this.savedRoutes.add(route);
	}
	
	public String toString() 
	{
	return String.format("%s\n%s\n%s\n%s\n%s\n", id, firstName, lastName, adress, city);
	}
}



