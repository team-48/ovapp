package ovapp;

import java.time.LocalTime;
import java.util.ArrayList;

public class SubwayLine {

    private int subwayLine; // atributen
    private Stop vertrekLocatie;
    private Stop aankomstLocatie;
    private ArrayList <LocalTime>subwayList;

    public SubwayLine(int subwayLine, Stop vertrekLocatie, Stop aankomstLocatie, ArrayList <LocalTime> subwayList) { //dit zorgt ervoor dat we buslijn en route kunnen gebruiken/aanroepen
        this.subwayLine = subwayLine;
        this.vertrekLocatie = vertrekLocatie;
        this.aankomstLocatie = aankomstLocatie;
        this.subwayList = subwayList;

    }
    public int getSubwayLine() {
        return subwayLine;
    }

    public Stop getVertrek() {
        return vertrekLocatie;
    }

    public Stop getAankomst() {
        return aankomstLocatie;
    }

    public ArrayList getSubwayList() { return subwayList;}

    public String toPrintSubwayTime(LocalTime departureTime) {
        return String.format("SubwayLine %d %s %s %s\n", getSubwayLine(), vertrekLocatie.getStop(), aankomstLocatie.getStop(), departureTime);
    }

}